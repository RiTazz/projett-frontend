import api_service from "@/Authenticating/api.service";

const administration = {
    namespaced: true,
    state : () => ({
        userProfil : '',
        promotions : '',
    }),
    getters : {
        getUserProfil(state){
            return state.userProfil;
        },
        getPromotions(state){
            if (localStorage.getItem("promotions") != null)
            {
                return JSON.parse(localStorage.getItem("promotions"))
            }else{
                return state.promotions
            }
        },
    },
    mutations : {
        setUserProfil(state){
            api_service.get("user/infos").then((response) => {
                state.userProfil = response.data.data
            });
        },
        setPromotions(state, promotions){
            state.promotions = promotions
        },
    },
    actions :{
        initPromotions(context){
            api_service.get("promotions")
                .then(async (response) => {
                    localStorage.setItem("promotions", null)
                    localStorage.setItem("promotions", JSON.stringify(response.data.data))
                    await context.commit("setPromotions", response.data.data )
                });
        },
    }
}

export default administration;