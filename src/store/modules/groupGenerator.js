import api_service from "@/Authenticating/api.service";

const groupGenerator = {
    namespaced: true,
    state: () => ({
        students: [],
        chosenUeId : null,
    }),
    getters: {
        getStudents(state) {
            let studentsSorted = state.students.sort((s1, s2) =>
                (s1.last_name < s2.last_name) ? 1 : ((s1.last_name > s2.last_name) ? -1 : 0)
            )
            return state.students
        },
        getChosenIeId(state){
            return state.chosenUeId
        }
    },
    mutations: {
        setStudents(state, students) {
            state.students = students
        },
        setchosenUEId(state, id){
            state.chosenUeId = id
        }
    },
    actions: {
        students(context, students) {
            context.commit("setStudents", students)
        },
        chosenUEId(context, id){
            context.commit("setchosenUEId", id)
        }
    }
}

export default groupGenerator;