import api_service from "@/Authenticating/api.service";

const course = {
    namespaced: true,
    state: () => ({
        usersOfParcoursToEdit: '',
        parcoursEdit: {
            name: null,
            sousPromotion: null,
        },
        courses: '',
        blocsOfParcoursToEdit: '',
        campagnes: null,
        students: null,
        studentsAllowedToSub: null,
        promotion: null,
        currentBlocks: null,
        actionForForm: false
    }),
    getters: {
        parcoursEdit(state) {
            state.parcoursEdit.sousPromotion = state.parcoursEdit?.promotion?.id
            state.parcoursEdit.full_name = state.parcoursEdit?.name +
                " " + state.parcoursEdit?.promotion?.name +
                "  "+ state.parcoursEdit?.promotion?.year
            return state.parcoursEdit;
        },
        campagnes(state) {
            return state.campagnes
        },
        students(state) {
            return state.students
        },
        studentsAllowedToSub(state) {
            if (state.studentsAllowedToSub != null) {
                return state.studentsAllowedToSub.filter(s => s.role == "student" && s.promotion.id == state.promotion.id)
            } else {
                return []
            }
        },
        promotion(state) {
            return state.promotion
        },
        actionForForm(state){
            return state.actionForForm
        }
    },
    mutations: {
        setCourses(state, courses) {
            state.courses = courses
        },
        parcoursEdit(state, id) {
            state.parcoursEdit = id;
        },
        campagnes(state, campagnes) {
            state.campagnes = campagnes
        },
        students(state, students) {
            state.students = students
        },
        studentsAllowedToSub(state, students) {
            state.studentsAllowedToSub = students
        },
        promotion(state, promotion) {
            state.promotion = promotion
        },
        actionForForm(state, action) {
            state.actionForForm = action
        }
    },
    actions: {
        initCourses(context) {
            let courses = []
            api_service.get("parcours")
                .then(async (responseCourse) => {
                    responseCourse.data.data.forEach(course => {
                        api_service.get("parcours/" + course.id + "/promotion").then((response) => {
                            course.name = course.name + " " + response.data.data.name + " " + response.data.data.year
                            courses.push(course)
                        })
                    })
                    localStorage.setItem("courses", null)
                    localStorage.setItem("courses", JSON.stringify(courses))
                    context.commit("setCourses", courses)
                });
        },
        parcoursEdit(context, id) {
            if (id !== null) {
                api_service.get("parcours/" + id).then((response) => {
                    context.commit("parcoursEdit", response.data.data);
                    context.commit("actionForForm", false)
                })
            } else {
                context.commit("parcoursEdit", {
                        name: null,
                        sousPromotion: null,
                    }
                )
                context.commit("actionForForm", true)
            }
        },
        campagnes(context, id) {
            api_service.get("parcours/" + id + "/campagnes")
                .then((response) => {
                    context.commit("campagnes", response.data.data)
                }).catch(e => {
                context.commit("campagnes", [])
            })
        },
        students(context, id) {
            api_service.get("parcours/" + id + "/users").then((response) => {
                context.commit("students", response.data.data)
            }).catch(e => {
                context.commit("students", [])
            })
        },
        studentsAllowedToSub(context, id) {
            api_service.get("user").then((response) => {
                context.commit("studentsAllowedToSub", response.data.data)
            })
        },
        promotion(context, id) {
            api_service.get("parcours/" + id + "/promotion").then((response) => {
                context.commit("promotion", response.data.data)
            })
        },
        actionForForm(context, action) {
            context.commit("actionForForm", action)
        }
    }
}

export default course;