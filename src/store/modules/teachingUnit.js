import api_service from "@/Authenticating/api.service";
import stringUtils from "convert-csv-to-json/src/util/stringUtils";
import store from "@/store";

const teachingUnit = {
    namespaced: true,
    state: () => ({
        ues: null,
        chosenUe: null,
        uesOfBlock: null,
        students: null,
        StudentsAllowedToSubscribe: null,
        initialStudentsOfChosenUnit: null,
        finalStudentsOfChosenUnit: null,
        createTeachingUnit: null,
        UnitToEdit: {
            number: null,
            name: null,
            full_name: null,
            capacity: null,
            is_open: false,
            is_optional: false,
            block: null
        },
        courses: null,
        groups: null
    }),
    getters: {
        getGroups(state) {
            if(state.groups !=null)
            {
                return state.groups
            }else{
                return []
            }
        },
        getUesSelectable(state) {
            let arrSelectable = []
            if (state.ues != null && state.uesOfBlock != null) {
                if (state.uesOfBlock.length === 0) {
                    return state.ues
                }
                state.ues.forEach(elementUes => {
                    let existe = false
                    state.uesOfBlock.forEach(elementUesBlock => {
                        if (elementUes.id === elementUesBlock.id) {
                            existe = true
                        }
                    })
                    if (!existe) {
                        arrSelectable.push(elementUes)
                    }
                });
                return arrSelectable
            }
        },
        initialStudentsOfChosenUnit(state) {
            return state.initialStudentsOfChosenUnit
        },
        finalStudentsOfChosenUnit(state) {
            return state.finalStudentsOfChosenUnit
        },
        students(state) {
            if (state.students == null) {
                return JSON.parse(localStorage.getItem("students"))
            } else {
                return state.students
            }
        },
        getCreateTeachingUnit(state) {
            return state.createTeachingUnit
        },
        getUnitToEdit(state) {
            return state.UnitToEdit
        },
        courses(state) {
            return state.courses
        }
    },
    mutations: {
        setUes(state, ues) {
            state.ues = ues
        },
        setChosenUnit(state, id) {
            state.chosenUe = id
        },
        setUesOfBlock(state, ues) {
            state.uesOfBlock = ues
        },
        initialStudentsOfChosenUnit(state, students) {
            state.initialStudentsOfChosenUnit = students
        },
        finalStudentsOfChosenUnit(state, students) {
            state.finalStudentsOfChosenUnit = students
        },
        students(state, students) {
            state.students = students
        },
        setCreateTeachingUnit(state, action) {
            state.createTeachingUnit = action
        },
        setUnitToEdit(state, unit) {
            state.UnitToEdit = unit
        },
        courses(state, courses) {
            state.courses = courses
        },
        setGroups(state, groupes){
            state.groups = groupes
        }
    },
    actions: {
        initUes(context) {
            api_service.get("ues").then(async (response) => {
                await context.commit("setUes", response.data.data)
            })
        },
        initChosenUe(context, id) {
            context.commit("setChosenUnit", id)
        },
        initUesOfBLock(context, id) {
            api_service.get("/blocs/" + id + "/ues")
                .then(async (response) => {
                    await context.commit("setUesOfBlock", response.data.data)
                });
        },
        initialStudentsOfChosenUnit(context, id) {
            api_service.get("/ues/" + id + "/users/initial")
                .then(async (response) => {
                    await localStorage.setItem("initialStudentsOfChosenUnit", null)
                    await localStorage.setItem("initialStudentsOfChosenUnit", JSON.stringify(response.data.data))
                    await context.commit("initialStudentsOfChosenUnit", response.data.data)
                })
        },
        finalStudentsOfChosenUnit(context, id) {
            api_service.get("/ues/" + id + "/users/final")
                .then(async (response) => {
                    await localStorage.setItem("finalStudentsOfChosenUnit", null)
                    await localStorage.setItem("finalStudentsOfChosenUnit", JSON.stringify(response.data.data))
                    await context.commit("finalStudentsOfChosenUnit", response.data.data)
                })
        },
        students(context, id) {
            api_service.get("/ues/" + id + "/users")
                .then(async (response) => {
                    await localStorage.setItem("students", null)
                    await localStorage.setItem("students", JSON.stringify(response.data.data))
                    await context.commit("students", response.data.data)
                })
        },
        initActionForForm(context, action) {
            context.commit("setCreateTeachingUnit", action)
        },
        initUnitToEdit(context, id) {
            if (id !== null) {
                api_service.get("ues/" + id).then(async (response) => {
                    context.commit("setUnitToEdit", response.data.data)
                    context.dispatch("courses",id)
                })
            } else {
                context.commit("setUnitToEdit", {
                    number: null,
                    name: null,
                    full_name: null,
                    capacity: null,
                    is_open: false,
                    is_optional: false,
                    block: null
                })

            }
        },
        courses(context, id) {
            api_service.get("ues/" + id + "/blocs").then((response) => {
                let courses = []
                response.data.data.forEach(b => {
                    courses.push(b.parcours)
                })
                context.commit("courses", courses)
            })
        },
        groups(context, id){
            api_service.get('ues/'+id+'/groupes').then(async response => {
                let groupes = response.data.data
                for (const g of groupes) {
                    g.chosenStudents = []
                    await api_service.get('groupes/'+g.id+'/users').then(response => {
                        g.ue.students = response.data.data
                    })
                }
                context.commit("setGroups",groupes)
            }).catch(e=>{
              context.commit("setGroups", [])
            })
        }
    }
}

export default teachingUnit;