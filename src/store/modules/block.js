import api_service from "@/Authenticating/api.service";

const block = {
    namespaced: true,
    state: () => ({
        chosenBlock: '',
        blocks: null,
        blocksOfUe: null,
        blockToEdit: {
            name: null,
            course: null,
            nb_optional_ue: null
        },
        ActionForForm: null
    }),
    getters: {
        getUes(state) {
            return state.chosenBlock
        },
        getBlocks(state) {
            if (localStorage.getItem("blocks") != null) {
                return JSON.parse(localStorage.getItem("blocks"))
            } else {
                return state.blocks
            }
        },
        getBlocksSelectable(state) {
            let arr = []
            if (state.blocks != null && state.blocksOfUe != null) {
                if (state.blocksOfUe.length === 0) {
                    return state.blocks
                }
                state.blocks.forEach(elementBlocks => {
                    let existe = false
                    state.blocksOfUe.forEach(elementBlocksUe => {
                        if (elementBlocks.id === elementBlocksUe.id) {
                            existe = true
                        }
                    });
                    if (!existe) {
                        arr.push(elementBlocks)
                    }
                });
                return arr
            }
        }
    },
    mutations: {
        setChosenBlock(state, id) {
            state.chosenBlock = id
        },
        setBlocks
            (state, blocks) {
            state.blocks = blocks
        }
        ,
        setBlocksOfUe(state, UEs) {
            state.blocksOfUe = UEs
        }
        ,
        blockToEdit(state, block) {
            state.blockToEdit = block
        }
        ,
        ActionForForm(state, action) {
            state.ActionForForm = action
        }
    },
    actions: {
        chosenBlock(context, id) {
            context.commit("setChosenBlock", id)
        }
        ,
        initAllBlocks(context) {
            api_service.get("blocs").then(async (response) => {
                localStorage.setItem("blocks", null)
                localStorage.setItem("blocks", JSON.stringify(response.data.data))
                await context.commit("setBlocks", response.data.data)
            })
        }
        ,
        initBlocksOfUe(context, id) {
            api_service.get("ues/" + id + "/blocs").then(async (response) => {
                await context.commit("setBlocksOfUe", response.data.data)
            })
        }
        ,
        blockToEdit(context, id) {
            if (id != null) {
                api_service.get("blocs/" + id).then(async (response) => {
                    let bloc = response.data.data
                    await api_service.get("blocs/" + id + "/parcours").then(async (response) => {
                        bloc.course = response.data.data
                        await context.commit("blockToEdit", bloc)
                    })
                })
            } else {
                context.commit("blockToEdit", {
                    name: null,
                    course: null,
                    nb_optional_ue: null
                })
            }
        }
        ,
        ActionForForm(context, action) {
            context.commit("ActionForForm", action)
        }
    }
    ,

}

export default block;