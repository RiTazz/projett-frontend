import apiService from "@/Authenticating/api.service";
import store from "@/store";

const student = {
    namespaced: true,
    state: () => ({
        uesOfChosenBlock: null,
        selectedBlock: {},
        usersInitial: null,
        campaing : null
    }),
    getters: {
        uesToChoice(state) {
            //Si la liste des insciption de l'élève n'est pas null
            //Je cherche les ues de ce bloc
            //S'ils éxistes alors je renseigne le champs "chosenOrder"
            if (state.uesOfChosenBlock !== null) {
                return  state.uesOfChosenBlock
            } else {
                return []
            }
        },
        selectedBlock(state) {
            return state.selectedBlock
        },
        usersInitial(state) {
            return state.usersInitial
        },
        campaing(state){
            if(state.campaing !== null)
                return state.campaing[0]
            return null
        }
    },
    mutations: {
        uesOfChosenBlock(state, ues) {
            state.uesOfChosenBlock = ues
        },
        selectedBlock(state, block) {
            state.selectedBlock = block
        },
        usersInitial(state, usersInitial) {
            state.usersInitial = usersInitial
        },
        setCampaing(state, campaign){
            state.campaing = campaign
        }
    },
    actions: {
        uesOfChosenBlock(context, id) {
            apiService.get("blocs/" + id + "/ues").then((response) => {
                let uesOfChosenBlock = response.data.data
                let ues = uesOfChosenBlock.filter(u => (u.is_optional === "true"))
                apiService.get('user/' + store.getters.getUserInfo.id + '/initial').then(async (response) => {
                    let usersInitial = response.data.data
                    for (const ueO of ues) {
                        ueO.chosenOrder = usersInitial.filter(u => u.ue.id === ueO.id)[0]?.choice_number
                        await apiService.get('ues/' + ueO.id + '/users/initial').then(response => {
                            ueO.subUserLength = response.data.data.filter(choice => choice.choice_number === 1)
                        })
                    }
                    await context.commit("uesOfChosenBlock", ues)
                })
            })
        },
        selectedBlock(context, id) {
            context.dispatch("uesOfChosenBlock", id)
            context.dispatch("usersInitial", store.getters.getUserInfo.id)
            apiService.get("blocs/" + id).then((response) => {
                context.commit("selectedBlock", response.data.data)
            })
        },
        usersInitial(context, id) {
            apiService.get('user/' + id + '/initial').then((response) => {
                context.commit("usersInitial", response.data.data)
            })
        },
        campaing(context) {
            if(store.getters.getUserInfo===null)
            {
                alert('Veuillez vous reconnecter !')
            }else {
                apiService.get('parcours/'+store.getters.getUserInfo.parcours.id+'/campagnes'+'?filter=active').then((response)=>{
                    context.commit('setCampaing', response.data.data)
                }).catch(e => {
                    context.commit('setCampaing',null)
                })
            }
        }
    }
}

export default student;