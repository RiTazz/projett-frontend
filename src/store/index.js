import {createStore} from "vuex";
import api_service from "@/Authenticating/api.service";
import administration from "@/store/modules/administration";
import course from "@/store/modules/parcours";
import teachingUnit from "@/store/modules/teachingUnit"
import block from "@/store/modules/block";
import student from "@/store/modules/student";
import groupGenerator from "@/store/modules/groupGenerator";
import Auth from "@/Authenticating/Auth";

export default createStore({
    state: {
        userToEdit: '',
        userInfo: null
    },
    getters: {
        getUserIdToEdit(state) {
            return state.userToEdit;
        },
        getUserInfo(state) {
            if (state.userInfo === null) {
                return JSON.parse(localStorage.getItem("userInfo"))
            } else {
                return state.userInfo
            }
        }
    },
    mutations: {
        setUserToEdit(state, user) {
            state.userToEdit = user
        },
        setUserInfo(state, userInfo) {
            state.userInfo = userInfo
        }
    },
    actions: {
        initUserInfo(context) {
           // Auth.methods.checkIfLogged()
            if(window.localStorage.getItem("access_token") !== 'null' && typeof window.localStorage.getItem("access_token") !== 'undefined'){
                return api_service.get("user/infos")
                    .then((Response) => {
                        localStorage.setItem("userInfo", null)
                        localStorage.setItem("userInfo", JSON.stringify(Response.data.data))
                        context.commit("setUserInfo", Response.data.data)
                    }).catch((e) => {
                        localStorage.setItem("userInfo", null)
                        context.state.userInfo = null
                        return context.state.userInfo
                    })
            }else {
                localStorage.setItem("userInfo", null)
                context.commit("setUserInfo", null)
            }
        }
    },
    modules: {
        administration,
        course,
        teachingUnit,
        block,
        student,
        groupGenerator
    },
});