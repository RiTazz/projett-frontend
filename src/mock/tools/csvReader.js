export function readFile(file) {
    let reader = new FileReader();

    return new Promise((resolve, reject) => {
        reader.onerror = () => {
            reject(new Error('There was an error reading the file!'));
        };
        reader.onload = () => resolve(reader.result);
        reader.readAsText(file);
    });
}

export function parseCSV(file) {
    // create an array with the file rows as elements
    let dataRows = file.split("\r\n");
    // remove the first row which represents the table header
    dataRows.shift();

    return dataRows.map((row, index) => {
        let data = row.split(';');
        return {
            first_name: data[0],
            last_name: data[1],
            email: data[2],
            promotion: data[3],
            year: data[4]
        };
    });
}