export default (server) => {
    server.get("/parcours",(schema,request) => {
        return {
            data : schema.db.courses
        }
    })
    server.get("/parcours/:id/promotion",(schema,request) => {
        let promotionId = schema.courses.find(request.params.id).promotionId
        return {
            data : schema.promotions.find(promotionId)
        }
    })
    server.post("/parcours", (schema, request) => {
        return schema.courses.create(JSON.parse(request.requestBody)).attrs
    })
    server.put("parcours/:idP/blocs/add/:idB", (schema, request) => {
        return {
            data: schema.blocks.find(request.params.idB)
                .update({course: schema.courses.find(request.params.idP).attrs})
        }
    })
    server.put("/parcours/:id", (schema, request) => {
        return schema.courses.find(request.params.id)
            .update(JSON.parse(request.requestBody))
    })
    server.put("/parcours/:idPa/promotion/:idPr", (schema, request) => {
        return schema.courses.find(request.params.id)
            .update(JSON.parse(request.requestBody))
    })

    server.delete("/parcours/:id", (schema, request) => {
        return schema.courses.find(request.params.id).destroy()
    })
};