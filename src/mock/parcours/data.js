export default () => [
    {
        'id' : 1,
        'name': 'S1',
        'promotionId' : "fc349e8bf0c17de6567ee2ccb99a464b"
    },
    {
        'id' : 2,
        'name': 'S2 Stage',
        'promotionId' : "fc349e8bf0c17de6567ee2ccb99a464b"
    },
    {
        'id' : 3,
        'name': 'S2 Alternant',
        'promotionId' : "fc349e8bf0c17de6567ee2ccb99a464b"
    },
    {
        'id' : 4,
        'name': 'OSIE',
        'promotionId' : "94e254583d8c75303439e1bcc8448841"
    },
    {
        'id' : 5,
        'name': 'SIO',
        'promotionId' : "94e254583d8c75303439e1bcc8448841"
    },
    {
        'id' : 6,
        'name': 'SID',
        'promotionId' : "94e254583d8c75303439e1bcc8448841"
    }
];