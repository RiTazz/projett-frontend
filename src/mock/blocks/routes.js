export default (server) => {
    server.get("/blocs", (schema, request) => {
        return {
            data: schema.blocks.all().models
        }
    })
    server.get("/blocs/:id/ues", (schema, request) => {
        return {
            data: schema.blocks.find(request.params.id).teachingUnits
        }
    })
    server.get("/blocs/:id/parcours", (schema, request) => {
        return {
            data: schema.blocks.find(request.params.id).course
        }
    })
    server.post("/blocs", (schema, request) => {
        return {
            data: schema.blocks.create(JSON.parse(request.requestBody)).attrs
        }
    })
    server.put("/blocs/:id", (schema, request) => {
        return schema.blocks.find(request.params.id)
            .update(JSON.parse(request.requestBody))
    })
    server.put("/blocs/:idBlock/ues/remove/:idUe", (schema, request) => {
        let arr = schema.blocks.find(request.params.idBlock).teachingUnits
        for (let key in arr) {
            if (arr[key].id === request.params.idUe) {
                arr.splice(key, 1)
            }
        }
        return schema.blocks.find(request.params.idBlock).update({teachingUnits: arr})
    })
    server.put("/blocs/:idBlock/ues/add/:idUe", (schema, request) => {
        let arr = schema.blocks.find(request.params.idBlock).teachingUnits
        arr.push(schema.teachingUnits.find(request.params.idUe).attrs)
        return schema.blocks.find(request.params.idBlock).update({teachingUnits: arr})
    })
    server.delete("/blocs/:id", (schema, request) => {
        return schema.blocks.find(request.params.id).destroy()
    })
};