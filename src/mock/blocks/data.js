export default () => [
    {
        'id': '1',
        'name': 'Informatique',
        'nb_ues_choix': 3,
        'course': {
            'id': 1,
            'name': 'S1',
        },
        teachingUnits : [
            {
                'id': '1',
                'name': 'Architecture Système d\'information',
                'number': '0',
                'full_name': "INFO_04 Architecture Système d\'information",
                'capacity': "25",
                'is_optional': true,
                'is_open': true,
            }
        ]
    },
    {
        'id': '2',
        'name': 'Domaine d\'Application',
        'nb_ues_choix': 2,
        'course': {
            'id': 2,
            'name': 'S2 Stage',
        },
        teachingUnits : [
            {
                'id': '2',
                'name': 'Architecture client serveur',
                'number': '0',
                'full_name': "ISI_0 Architecture client serveur",
                'capacity': "25",
                'is_optional': true,
                'is_open': true
            }
        ]
    },
    {
        'id': '3',
        'name': 'Compétences Transverses',
        'nb_ues_choix': 2,
        'course': {
            'id': 3,
            'name': 'S2 Alternant',
        },
        teachingUnits : [
            {
                'id': '3',
                'name': 'Marketing orienté Web',
                'number': '0',
                'full_name': "GEO_06 Marketing orienté Web",
                'capacity': "25",
                'is_optional': true,
                'is_open': true
            }
        ]
    }
];