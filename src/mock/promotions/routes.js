export default (server) => {
    server.get("promotions", (schema) => {
        return {
            data: schema.db.promotions
        }
    })
}