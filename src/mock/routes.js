import  loadUsersRoutes from '@/mock/users/routes'
import  loadPromotionsRoutes from '@/mock/promotions/routes'
import  loadAuthenticationRoutes from  '@/mock/authentifications/routes'
import  loadTeachingUnitsRoutes from '@/mock/teachingUnits/routes'
import  loadBlocksRoutes from '@/mock/blocks/routes'
import loadParcoursRoutes from '@/mock/parcours/routes'

export default (server) => {
    server.namespace = "api";
    loadUsersRoutes(server);
    loadPromotionsRoutes(server);
    loadAuthenticationRoutes(server);
    loadTeachingUnitsRoutes(server);
    loadBlocksRoutes(server);
    loadParcoursRoutes(server);
}