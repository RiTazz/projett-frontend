import {belongsTo, createServer, hasMany, JSONAPISerializer, Model, RestSerializer} from "miragejs"
import loadRoutes from '@/mock/routes'
import loadUsers from '@/mock/users/data'
import loadPromotions from '@/mock/promotions/data'
import loadAuthentications from '@/mock/authentifications/data'
import loadteachingUnits from '@/mock/teachingUnits/data'
import loadBlocs from '@/mock/blocks/data'
import loadParcours from '@/mock/parcours/data'


export function makeServer({ environment = "development" } = {}) {
    return createServer({
        environment,
        models: {
            user : Model.extend({
                    promotion : belongsTo,
                    authentication : belongsTo,
                    teachingUnits : hasMany
                }
            ),
            promotion: Model.extend({
                users : hasMany,
                blocks : hasMany
            }),
            authentication : Model.extend({
                user : belongsTo
            }),
            teachingUnit : Model.extend({
                block : belongsTo,
                students : hasMany
            }),
            block : Model.extend({
                promotion :  belongsTo,
                teachingUnits : hasMany
            }),
            course : Model.extend({
                promotion : belongsTo
            }),
        },
        seeds(server) {
            server.db.loadData({
                    users : loadUsers(),
                    promotions : loadPromotions(),
                    authentications : loadAuthentications(),
                    courses : loadParcours(),
                    teachingUnits : loadteachingUnits(),
                    blocks : loadBlocs(),
            })
        },
        routes(type, attributes) {
            loadRoutes(this)
        },
        /*serializers:{
            bloc : RestSerializer.extend({
                include: ['course'],
                embed: true,
            })
        }*/
    })
}