export default (server) => {
    server.post("/login", (schema,request) => {
        return  schema.users.findBy({email : JSON.parse(request.requestBody).email }).attrs.authentication
    })
    server.post("/logout",(schema) => {
        return new Response()
    })
    server.post("/refresh", (schema) => {
        return new Response()
    })
}