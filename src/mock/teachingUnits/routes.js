export default (server) => {
    server.get("/ues",(schema,request) => {
        return {
            data : schema.db.teachingUnits
        }
    })
    server.get("/ues/:id",(schema,request) => {
        return {
            data : schema.db.teachingUnits.find(request.params.id)
        }
    })
    server.get("/ues/:id/users/initial/",(schema,request) => {
        return {
            data : schema.db.teachingUnits.find(request.params.id).students
        }
    })
    server.get("/ues/:id/users/final/",(schema,request) => {
        return {
            data : schema.db.teachingUnits.find(request.params.id).students
        }
    })
    server.get("/ues/:idUe/users",(schema,request) => {
        return {
            data : schema.db.users.where({role: "student"})
        }
    })
    server.post("/ues", (schema, request) => {
        let ue =  JSON.parse(request.requestBody)
        ue.students = []
        return {
            data : schema.teachingUnits.create(ue).attrs
        }
    })
    server.put("/ues/:id", (schema, request) => {
        return schema.teachingUnits.find(request.params.id)
            .update(JSON.parse(request.requestBody))
    })
    server.put("/ues/:idUe/users/initial/remove/:id", (schema,request) =>{
        return {}

    })
    server.put("/ues/:idUe/users/initial/remove/:id", (schema,request) =>{
        return {}
    })
    server.delete("/ues/:id", (schema, request) => {
        return schema.teachingUnits.find(request.params.id).destroy()
    })
};