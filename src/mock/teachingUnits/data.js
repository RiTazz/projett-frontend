export default () => [
    {
        'id' : '1',
        'name': 'INFO',
        'number': '18',
        'full_name': "Architecture des SI 2 ",
        'capacity': "25",
        'is_optional': true,
        'is_open' : true,
        'students': [
            {
                "id": "3",
                "first_name": "antoni",
                "last_name": "capart",
                "email": "antoni.capart@gmail.com",
                "role": "student",
                "first_connection": 1,
                "commentary": "je suis en coloc avec d.KEKLIK",
                "promotion": {
                    "id": "94e254583d8c75303439e1bcc8448841",
                    "name": "M2",
                    "year": 2022
                },
            }
        ]
    },
    {
        'id' : '2',
        'name': 'ISI',
        'number': '1',
        'full_name': "Architecture client serveur",
        'capacity': "25",
        'is_optional': true,
        'is_open' : true,
        'students': [
            {
                "id": "2",
                "first_name": "David",
                "last_name": "keklik",
                "email": "david@azeazed.com",
                "role": "student",
                "first_connection": 1,
                "commentary": null,
                "promotion": {
                    "id": "7a35b9c7a7db3f7cdf6ccafc69bf328a",
                    "name": "YOLO",
                    "year": 2000
                }
            }
        ],
        "blockId": "1"

    },
    {
        'id' : '3',
        'name': 'GEO',
        'number': '6',
        'full_name': "Marketing orienté Web",
        'capacity': "25",
        'is_optional': true,
        'is_open' : true,
        'students': []
    }
];