import {parseCSV, readFile} from "@/mock/tools/csvReader";

export default (server) => {
    server.get("user/infos", (schema, request) => {
        let Authorization = request.requestHeaders.Authorization.split(" ")
        let auth =  schema.authentications.findBy({
            token_type: Authorization[0],
            access_token :   Authorization[1],
        }).attrs
        return {
            data: schema.authentications.findBy({
                token_type: Authorization[0],
                access_token :   Authorization[1],
            }).attrs.user
        }
    })
    server.get("/user", (schema) => {
        return {
            data: schema.db.users
        }
    })
    server.post("/user", (schema, request) => {
        let userRequested = JSON.parse(request.requestBody)
        userRequested.promotion = schema.findOrCreateBy('promotion', {
            name: userRequested.promotion,
            year: userRequested.promotion_year
        }).attrs;
        userRequested.authentication = null
        return schema.users.create({
            "first_name": userRequested.first_name,
            "last_name": userRequested.last_name,
            "email": userRequested.email,
            "role": userRequested.role,
            "first_connection": 1,
            "commentary": null,
            "promotion": userRequested.promotion,
            "authentication" : null
        },)
    })
    server.post("user/reset_password", (schema, request) => {
        return schema.users.find(JSON.parse(request.requestBody).userId)
    })
    server.post("/user/import", async (schema, request) => {
        let file = request.requestBody.get('file');
        let fileContents = await readFile(file);

        let usersData = parseCSV(fileContents)

        for (let key = 0; key < usersData.length - 1; key++) {
            let user = usersData[key]
            user.promotion = schema.findOrCreateBy('promotion', {
                name: user.promotion,
                year: user.year
            }).attrs;
            schema.users.create({
                first_name: user.first_name,
                last_name: user.last_name,
                email: user.email,
                role: 'student',
                promotion: user.promotion
            })
        }
    })
    server.put("/user/:id", (schema, request) => {
        return schema.users.find(request.params.id)
            .update(JSON.parse(request.requestBody))
    })
    server.delete("/user/:id", (schema, request) => {
        return schema.users.find(request.params.id).destroy()
    })
}