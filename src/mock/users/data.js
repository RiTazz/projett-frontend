export default () =>
    [
        {
            "id": "1",
            "first_name": "admin_first_name",
            "last_name": "admin_last_name",
            "email": "admin@test.test",
            "role": "admin",
            "first_connection": 1,
            "commentary": null,
            "promotion": null,
            "authentication" : {
                "id" : "0",
                "token_type": "Bearer",
                "expires_in": 86400,
                "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiOGNjZmQ5NTcxMTZjYjEzZWY3MGYwOGI5MTU5MWQwZWZjNTM5YTBmZDRjNzcyZDlhM2NkNTM4ZWZiZmI3NWNlMzdlODYyNzU0NmEwNmI0NzYiLCJpYXQiOjE2NDg4MDA0MTMuNjk3Njc1LCJuYmYiOjE2NDg4MDA0MTMuNjk3Njc5LCJleHAiOjE2NDg4ODY4MTMuNjkxMzc0LCJzdWIiOiIxIiwic2NvcGVzIjpbIioiXX0.b45SWem-0OMSJpUmGmszZK-XtvwAmuXVFUnDYZoxTvk-houG3l75UZkuuEzqIdI-sHPVxqPR-ECWXm-8BcuuT0j_l4t5dQg2bhol9_52PmkD-5MhIlfk7j8Km78q9pLy5zJX472revo-j9pqeksGwmz6TOZTny-maUcM9WWQeWtPv7MbqFzu5a9yPn5vwwiviaIEciPosBMbB-JKjlPmf36DIQvQne1LSNjNQYIQZMnoFOA8Q7IohrjAHLY8S8tUe5K903EaScsWuQItNK2szigPcMlmhiyyFwhHcrlNdphr49-6UoKG8c-aXmXiFmzu-BIy0lHobeu3BnP9RFiNzB9p7Jnmv6GFSKLIl0z_p8pesjlls_Md24v2P1aMkBFWRTQTYIPC1FXlep4QaOaFqQ6uQSFmjqGHgU9eUW57fDrqG0gOendZ_I7IFcW1IKry2uNshIxMm49Csu1XyQRp9RVyhC4nKFCrHB3IY3ynXq80vK4Grdg88Xo8E0gs9GtlIbsxY9SCExv68wtrGH6zUZi4D9QGhlJJiPw1hmWr9EphK5PrH52MjUwBDuS9zx4A30tpvT9U63ip-lqO1j6T7HD_JQmAAgLJxOpdPgunR7InpJOySngHWLOAu_8pqNprQBN_Xq8mPRii_UTcXJr2HYb_hdXIenTpx821C3kdOVM",
                "refresh_token": "def502006d369888b44502ef7e836c259d150311ebd91ff786cf156a02ff538b27409d9d5a882aa3032079b132cfe68af70386f3bc125fb1e0aa9aa25eaa1ff99cd948bf2ffcce41ead19f74c6ff84029dc44a131b926f18245b5459dc3bdc3a2809d860f96e77b565801248887469903bb18005ea7f1d25287e2c1222fcc27e3a1037260b7a7d724fbffd3f2607dc7e91fed1c45aa0cad9222e1ff9d85995540231005489f6b43a71b45d81b5aa7aa5b88245a5428983787cd98844859420bdb0b8d92e282c18d80521f0049ec2cbe385e66ad9582445b19f75a1514bd0e67badf5b87da62aff991e249bd94cdcdeb859c5dc5fb4b9fd6c61f500853c371ace887205e05e72a3144656c89249988691d1d8d9873d5175cf5bbd3fd184190bbea07bc68ddc15defe130499dac7bdcb25d6c341184a330aceb4f3146b012e158a80ae88a01e195da4036c8b5fc2c81ce84950c9cbf92eed6f6a50476a54d45f806d1c9fe1"
            }
        },
        {
            "id": "2",
            "first_name": "David",
            "last_name": "keklik",
            "email": "david@azeazed.com",
            "role": "student",
            "first_connection": 1,
            "commentary": null,
            "promotion": {
                "id": "7a35b9c7a7db3f7cdf6ccafc69bf328a",
                "name": "YOLO",
                "year": 2000
            },
            "authentication" : {
                "id" : "1",
                "token_type": "Bearer",
                "expires_in": 86400,
                "access_token": "David",
                "refresh_token": "def502006d369888b44502ef7e836c259d150311ebd91ff786cf156a02ff538b27409d9d5a882aa3032079b132cfe68af70386f3bc125fb1e0aa9aa25eaa1ff99cd948bf2ffcce41ead19f74c6ff84029dc44a131b926f18245b5459dc3bdc3a2809d860f96e77b565801248887469903bb18005ea7f1d25287e2c1222fcc27e3a1037260b7a7d724fbffd3f2607dc7e91fed1c45aa0cad9222e1ff9d85995540231005489f6b43a71b45d81b5aa7aa5b88245a5428983787cd98844859420bdb0b8d92e282c18d80521f0049ec2cbe385e66ad9582445b19f75a1514bd0e67badf5b87da62aff991e249bd94cdcdeb859c5dc5fb4b9fd6c61f500853c371ace887205e05e72a3144656c89249988691d1d8d9873d5175cf5bbd3fd184190bbea07bc68ddc15defe130499dac7bdcb25d6c341184a330aceb4f3146b012e158a80ae88a01e195da4036c8b5fc2c81ce84950c9cbf92eed6f6a50476a54d45f806d1c9fe1"
            }
        },
        {
            "id": "3",
            "first_name": "antoni",
            "last_name": "capart",
            "email": "antoni.capart@gmail.com",
            "role": "student",
            "first_connection": 1,
            "commentary": "je suis en coloc avec d.KEKLIK",
            "promotion": {
                "id": "94e254583d8c75303439e1bcc8448841",
                "name": "M2",
                "year": 2022
            },
            "authentication" : {
                "id" : "2",
                "token_type": "Bearer",
                "expires_in": 86400,
                "access_token": "antoni",
                "refresh_token": "def502006d369888b44502ef7e836c259d150311ebd91ff786cf156a02ff538b27409d9d5a882aa3032079b132cfe68af70386f3bc125fb1e0aa9aa25eaa1ff99cd948bf2ffcce41ead19f74c6ff84029dc44a131b926f18245b5459dc3bdc3a2809d860f96e77b565801248887469903bb18005ea7f1d25287e2c1222fcc27e3a1037260b7a7d724fbffd3f2607dc7e91fed1c45aa0cad9222e1ff9d85995540231005489f6b43a71b45d81b5aa7aa5b88245a5428983787cd98844859420bdb0b8d92e282c18d80521f0049ec2cbe385e66ad9582445b19f75a1514bd0e67badf5b87da62aff991e249bd94cdcdeb859c5dc5fb4b9fd6c61f500853c371ace887205e05e72a3144656c89249988691d1d8d9873d5175cf5bbd3fd184190bbea07bc68ddc15defe130499dac7bdcb25d6c341184a330aceb4f3146b012e158a80ae88a01e195da4036c8b5fc2c81ce84950c9cbf92eed6f6a50476a54d45f806d1c9fe1"
            }
        },
    ];