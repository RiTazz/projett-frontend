import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Auth from './Authenticating/Auth'
import ServiceApi from '@/Authenticating/api.service'
import {makeServer} from "@/mock/server"

require("bootstrap")


if (process.env.NODE_ENV === "development") {
    //makeServer()
}
ServiceApi.init()


router.beforeEach(async (to, from, next) => {
    if (to.name === 'home' && !await Auth.methods.checkIfLogged()) next()
    if (to.name !== 'login' && !await Auth.methods.checkIfLogged()) next({ name: 'login' })
    else {
      next();
    }
  })

createApp(App).use(store).use(router).mount('#app')