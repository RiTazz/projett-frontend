import axios from "axios";
import store from "@/store";
import apiService from "@/Authenticating/api.service";

const AuthData = {
    methods: {
        setTokensInfo(access_token, token_type, refresh_token) {
            window.localStorage.setItem('access_token', access_token)
            window.localStorage.setItem('type_token', token_type)
            window.localStorage.setItem('refresh_token', refresh_token)
        },
        refreshToken() {
            let body = {
                refresh_token: window.localStorage.getItem('refresh_token')
            }
            apiService.post("refresh", body).then( (data) => {
                this.setTokensInfo(data.data.access_token,
                    data.data.token_type,
                    data.data.refresh_token)
            }).catch(() => {
                this.setTokensInfo(null, null, null)
                window.location.href = '/login'
            })
        },
        async login(body) {
            let vm = this
            await apiService.post("login", body).then(({data}) => {
                vm.setTokensInfo(data.access_token, data.token_type, data.refresh_token)
            }).catch(e => {
                vm.setTokensInfo(null, null, null)
                throw {connection : 'failed'};
            })
        },
        logout() {
            let vm = this
            apiService.post('logout')
                .then(() => {
                    this.setTokensInfo(null,null,null)
                    store.dispatch('initUserInfo',null)
                    alert('Vous avez été déconnecté')
                    window.location.href = '/'
                })
                .catch(() => {
                    this.setTokensInfo(null,null,null)
                    store.dispatch('initUserInfo', null)
                    alert('Vous avez été déconnecté')
                    window.location.href = '/'
                })
        },
        async checkIfLogged() {
            if(window.localStorage.getItem("access_token") === 'null')
            {
                return false
            }else{
                let connected = false
                await apiService.get('user/infos').then(()=>{
                    connected = true
                }).catch(e =>{
                    connected = false
                    AuthData.methods.setTokensInfo(null,null,null)
                    store.dispatch("initUserInfo")
                })
                return connected
            }
        }
    }
}
export default AuthData
