import axios from "axios";
import AuthData from "./Auth";

export default {
  init() {
    //AuthData.methods.refreshToken();
    axios.defaults.baseURL = process.env.VUE_APP_API_ENDPOINT;
    if(window.localStorage.getItem('type_token') !== 'null' && window.localStorage.getItem('access_token') !=='null')
      axios.defaults.headers["Authorization"] = window.localStorage.getItem('type_token') + " " + window.localStorage.getItem('access_token');
    axios.defaults.headers[ 'Accept'] = 'application/json'
    },
  getPdf(resource){
    this.init();
    return axios.get(resource, {responseType: 'arraybuffer',headers:{'Accept' : 'application/pdf'}});
  },
  get(resource) {
    this.init();
    return axios.get(resource);
  },

  post(resource, params) {
    this.init();
    return axios.post(resource, params);
  },

  update(resource, params){
    this.init();
    return axios.put(resource, params);  
  },

  import(ressource, params){
    this.init();
    axios.defaults.headers["Conent-Type"] = "multipart/form-data;charset=utf-8"; 
    return axios.post(ressource, params);
  },
  delete(ressource,params){
    this.init()
    return axios.delete(ressource,params)
  }
};